package com.example.rubiks.Concrete;

import com.example.rubiks.Interfaces.ICornerPermutationParitySubAlgo;

/**
 * Created by sspas on 10/6/2016.
 */
public class CornerPermutationParitySubAlgo implements ICornerPermutationParitySubAlgo {

    private final RubiksCube3x3 cube;

    public CornerPermutationParitySubAlgo(RubiksCube3x3 cube){
        this.cube = cube;
    }

    @Override
    public Corner3x3[] generateSolvedCornersWithPositions() {
        Corner3x3[] solvedCorners = new Corner3x3[8];
        generateTopSideSolvedCorners(solvedCorners);
        generateBottomSideSolvedCorners(solvedCorners);
        return solvedCorners;
    }

    private void generateBottomSideSolvedCorners(Corner3x3[] solvedCorners) {
        // Bottom Side3x3 Corners
        Corner3x3 bottomSideTopLeftCorner = new Corner3x3(cube.getLeftSide().getSideColor(), cube.getBottomSide().getSideColor(), cube.getBackSide().getSideColor());
        solvedCorners[4] = bottomSideTopLeftCorner;
        Corner3x3 bottomSideTopRightCorner = new Corner3x3(cube.getBackSide().getSideColor(), cube.getBottomSide().getSideColor(), cube.getRightSide().getSideColor());
        solvedCorners[5] = bottomSideTopRightCorner;
        Corner3x3 bottomSideBottomLeftCorner = new Corner3x3(cube.getFrontSide().getSideColor(), cube.getBottomSide().getSideColor(), cube.getLeftSide().getSideColor());
        solvedCorners[6] = bottomSideBottomLeftCorner;
        Corner3x3 bottomSideBottomRightCorner = new Corner3x3(cube.getRightSide().getSideColor(), cube.getBottomSide().getSideColor(), cube.getFrontSide().getSideColor());
        solvedCorners[7] = bottomSideBottomRightCorner;
    }

    private void generateTopSideSolvedCorners(Corner3x3[] solvedCorners) {
        // Top Side3x3 Corners
        Corner3x3 topSideTopLeftCorner = new Corner3x3(cube.getBackSide().getSideColor(), cube.getTopSide().getSideColor(), cube.getLeftSide().getSideColor());
        solvedCorners[0] = topSideTopLeftCorner;
        Corner3x3 topSideTopRightCorner = new Corner3x3(cube.getRightSide().getSideColor(), cube.getTopSide().getSideColor(), cube.getBackSide().getSideColor());
        solvedCorners[1] = topSideTopRightCorner;
        Corner3x3 topSideBottomLeftCorner = new Corner3x3(cube.getLeftSide().getSideColor(), cube.getTopSide().getSideColor(), cube.getFrontSide().getSideColor());
        solvedCorners[2] = topSideBottomLeftCorner;
        Corner3x3 topSideBottomRightCorner = new Corner3x3(cube.getFrontSide().getSideColor(), cube.getTopSide().getSideColor(), cube.getRightSide().getSideColor());
        solvedCorners[3] = topSideBottomRightCorner;
    }

    @Override
    public void generateCornersWithPositions() {
        Corner3x3[] solvedCorners = generateSolvedCornersWithPositions();
        for (int i = 0; i < cube.getCorners().length; i++){
            for (int j = 0; j < solvedCorners.length; j++){
                if (cube.getCorners()[i].isSameCorner(solvedCorners[j])){
                    cube.getCorners()[i].setCornerPos(j);
                    break;
                }
            }
        }
    }

    @Override
    public int getCornerSwaps() {
        generateCornersWithPositions();
        int cornerSwaps = 0;
        for (int i = 0; i < cube.getCorners().length; i++){
            for (int j = i + 1; j < cube.getCorners().length; j++){
                if(cube.getCorners()[i].getCornerPos() > cube.getCorners()[j].getCornerPos()){
                    cornerSwaps++;
                }
            }
        }
        return cornerSwaps;
    }
}
