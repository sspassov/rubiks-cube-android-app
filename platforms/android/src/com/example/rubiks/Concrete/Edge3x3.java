package com.example.rubiks.Concrete;

import android.graphics.Color;

import com.example.rubiks.Abstract.AbsEdge;

/**
 * Created by sspas on 10/11/2016.
 */
public class Edge3x3 extends AbsEdge {
    public Edge3x3(int topColor, int bottomColor) {
        super(topColor, bottomColor);
    }
}
