package com.example.rubiks.Concrete;

import com.example.rubiks.Interfaces.IEdgePermutationParitySubAlgo;

/**
 * Created by sspas on 10/6/2016.
 */
public class EdgePermutationParitySubAlgo implements IEdgePermutationParitySubAlgo {

    private final RubiksCube3x3 cube;

    public EdgePermutationParitySubAlgo(RubiksCube3x3 cube){
        this.cube = cube;
    }

    @Override
    public Edge3x3[] generateSolvedEdgesWithPositions() {
        Edge3x3[] solvedEdges = new Edge3x3[12];
        generateTopSideSolvedEdges(solvedEdges);
        generateMiddleSideSolvedEdges(solvedEdges);
        generateBottomSideSolvedEdges(solvedEdges);
        return solvedEdges;
    }

    private void generateBottomSideSolvedEdges(Edge3x3[] solvedEdges) {
        //Bottom Edges
        Edge3x3 bottomFrontEdge = new Edge3x3(cube.getBottomSide().getSideColor(), cube.getFrontSide().getSideColor());
        solvedEdges[8] = bottomFrontEdge;
        Edge3x3 bottomRightEdge = new Edge3x3(cube.getBottomSide().getSideColor(), cube.getRightSide().getSideColor());
        solvedEdges[9] = bottomRightEdge;
        Edge3x3 bottomBackEdge = new Edge3x3(cube.getBottomSide().getSideColor(), cube.getBackSide().getSideColor());
        solvedEdges[10] = bottomBackEdge;
        Edge3x3 bottomLeftEdge = new Edge3x3(cube.getBottomSide().getSideColor(), cube.getLeftSide().getSideColor());
        solvedEdges[11] = bottomLeftEdge;
    }

    private void generateMiddleSideSolvedEdges(Edge3x3[] solvedEdges) {
        //Middle Edges
        Edge3x3 frontRightEdge = new Edge3x3(cube.getFrontSide().getSideColor(), cube.getRightSide().getSideColor());
        solvedEdges[4] = frontRightEdge;
        Edge3x3 rightBackEdge = new Edge3x3(cube.getRightSide().getSideColor(), cube.getBackSide().getSideColor());
        solvedEdges[5] = rightBackEdge;
        Edge3x3 backLeftEdge = new Edge3x3(cube.getBackSide().getSideColor(), cube.getLeftSide().getSideColor());
        solvedEdges[6] = backLeftEdge;
        Edge3x3 leftFrontEdge = new Edge3x3(cube.getLeftSide().getSideColor(), cube.getFrontSide().getSideColor());
        solvedEdges[7] = leftFrontEdge;
    }

    private void generateTopSideSolvedEdges(Edge3x3[] solvedEdges) {
        //Top Edges
        Edge3x3 topFrontEdge = new Edge3x3(cube.getTopSide().getSideColor(), cube.getFrontSide().getSideColor());
        solvedEdges[0] = topFrontEdge;
        Edge3x3 topRightEdge = new Edge3x3(cube.getTopSide().getSideColor(), cube.getRightSide().getSideColor());
        solvedEdges[1] = topRightEdge;
        Edge3x3 topBackEdge = new Edge3x3(cube.getTopSide().getSideColor(), cube.getBackSide().getSideColor());
        solvedEdges[2] = topBackEdge;
        Edge3x3 topLeftEdge = new Edge3x3(cube.getTopSide().getSideColor(), cube.getLeftSide().getSideColor());
        solvedEdges[3] = topLeftEdge;
    }

    @Override
    public void generateEdgesWithPositions() {
        Edge3x3[] solvedEdges = generateSolvedEdgesWithPositions();
        for (int i = 0; i < cube.getEdges().length; i++){
            for (int j = 0; j < solvedEdges.length; j++){
                if (cube.getEdges()[i].isSameEdge(solvedEdges[j])){
                    cube.getEdges()[i].setEdgePos(j);
                    break;
                }
            }
        }

    }

    @Override
    public int getEdgeSwaps() {
        generateEdgesWithPositions();
        int edgeSwaps = 0;
        for ( int i = 0; i < cube.getEdges().length; i++){
            for ( int j = i + 1; j < cube.getEdges().length; j++){
                if (cube.getEdges()[i].getEdgePos() > cube.getEdges()[j].getEdgePos()){
                    edgeSwaps++;
                }
            }
        }
        return edgeSwaps;
    }
}
