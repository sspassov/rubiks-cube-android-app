package com.example.rubiks.Interfaces;

import com.example.rubiks.Concrete.Corner3x3;

/**
 * Created by sspas on 10/6/2016.
 */
public interface ICornerPermutationParitySubAlgo {

    Corner3x3[] generateSolvedCornersWithPositions();

    void generateCornersWithPositions();

    int getCornerSwaps();
}
