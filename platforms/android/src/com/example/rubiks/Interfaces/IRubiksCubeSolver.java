package com.example.rubiks.Interfaces;

/**
 * Created by sspas on 11/17/2016.
 */
public interface IRubiksCubeSolver {

    boolean isRubiksCubeSolvable(String[] topColors, String[] bottomColors, String[] frontColors, String[] backColors, String[] leftColors, String[] rightColors);
}
