package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.Edge3x3;

import junit.framework.TestCase;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


/**
 * Created by sspas on 10/7/2016.
 */
public class Edge3x3Test extends TestCase{


    public void testTwoEdgesAreEqual(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        assertTrue(firstEdge.equals(secondEdge));
    }


    public void testTwoEdgesAreNotEqual(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        Edge3x3 secondEdge = new Edge3x3(Color.GREEN, Color.BLUE);
        assertFalse(firstEdge.equals(secondEdge));
    }


    public void testTwoEdgesAreSame(){
        Edge3x3 firstEdge = new Edge3x3(Color.BLUE, Color.RED);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        assertTrue(firstEdge.isSameEdge(secondEdge));
    }


    public void testTwoEdgesAreNotSame(){
        Edge3x3 firstEdge = new Edge3x3(Color.BLUE, Color.RED);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.GREEN);
        assertFalse(firstEdge.isSameEdge(secondEdge));
    }


    public void testTwoEdgesAreEqualWithPos(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        firstEdge.setEdgePos(1);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        secondEdge.setEdgePos(1);
        assertTrue(firstEdge.equalsWithPos(secondEdge));
    }


    public void testTwoEdgesAreNotEqualWithPos(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        firstEdge.setEdgePos(1);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        secondEdge.setEdgePos(2);
        assertFalse(firstEdge.equalsWithPos(secondEdge));
    }
}
