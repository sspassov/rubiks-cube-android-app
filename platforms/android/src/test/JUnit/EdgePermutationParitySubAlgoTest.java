package test.JUnit;

import android.graphics.Color;

import com.example.rubiks.Concrete.Edge3x3;
import com.example.rubiks.Concrete.EdgePermutationParitySubAlgo;
import com.example.rubiks.Concrete.RubiksCube3x3;

import junit.framework.TestCase;

import java.util.Arrays;




/**
 * Created by sspas on 10/7/2016.
 */
public class EdgePermutationParitySubAlgoTest extends TestCase{

    int[] topColors = new int[]{Color.WHITE, Color.rgb(255, 165, 0), Color.YELLOW, Color.RED, Color.BLUE, Color.YELLOW, Color.YELLOW, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
    int[] bottomColors = new int[]{Color.rgb(255, 165, 0), Color.BLUE, Color.YELLOW, Color.WHITE, Color.GREEN, Color.YELLOW, Color.RED, Color.RED, Color.WHITE};
    int[] frontColors = new int[]{Color.RED, Color.BLUE, Color.RED, Color.RED, Color.WHITE, Color.YELLOW, Color.BLUE, Color.WHITE, Color.RED};
    int[] backColors = new int[]{Color.BLUE, Color.GREEN, Color.WHITE, Color.GREEN, Color.YELLOW, Color.BLUE, Color.rgb(255, 165, 0), Color.YELLOW, Color.BLUE};
    int[] leftColors = new int[]{Color.BLUE, Color.GREEN, Color.GREEN, Color.RED, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.YELLOW, Color.WHITE, Color.GREEN};
    int[] rightColors = new int[]{Color.WHITE, Color.BLUE, Color.GREEN, Color.rgb(255, 165, 0), Color.RED, Color.GREEN, Color.GREEN, Color.WHITE, Color.rgb(255, 165, 0)};
    RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
    EdgePermutationParitySubAlgo algo = new EdgePermutationParitySubAlgo(cube);


    public void testGenerateSolvedEdgesWithPositions(){
        Edge3x3[] expectedSolvedEdges = new Edge3x3[12];
        Edge3x3 topFrontEdge = new Edge3x3(Color.BLUE, Color.WHITE);
        expectedSolvedEdges[0] = topFrontEdge;
        Edge3x3 topRightEdge = new Edge3x3(Color.BLUE, Color.RED);
        expectedSolvedEdges[1] = topRightEdge;
        Edge3x3 topBackEdge = new Edge3x3(Color.BLUE, Color.YELLOW);
        expectedSolvedEdges[2] = topBackEdge;
        Edge3x3 topLeftEdge = new Edge3x3(Color.BLUE, Color.rgb(255, 165, 0));
        expectedSolvedEdges[3] = topLeftEdge;
        Edge3x3 frontRightEdge = new Edge3x3(Color.WHITE, Color.RED);
        expectedSolvedEdges[4] = frontRightEdge;
        Edge3x3 rightBackEdge = new Edge3x3(Color.RED, Color.YELLOW);
        expectedSolvedEdges[5] = rightBackEdge;
        Edge3x3 backLeftEdge = new Edge3x3(Color.YELLOW, Color.rgb(255, 165, 0));
        expectedSolvedEdges[6] = backLeftEdge;
        Edge3x3 leftFrontEdge = new Edge3x3(Color.rgb(255, 165, 0), Color.WHITE);
        expectedSolvedEdges[7] = leftFrontEdge;
        Edge3x3 bottomFrontEdge = new Edge3x3(Color.GREEN, Color.WHITE);
        expectedSolvedEdges[8] = bottomFrontEdge;
        Edge3x3 bottomRightEdge = new Edge3x3(Color.GREEN, Color.RED);
        expectedSolvedEdges[9] = bottomRightEdge;
        Edge3x3 bottomBackEdge = new Edge3x3(Color.GREEN, Color.YELLOW);
        expectedSolvedEdges[10] = bottomBackEdge;
        Edge3x3 bottomLeftEdge = new Edge3x3(Color.GREEN, Color.rgb(255, 165, 0));
        expectedSolvedEdges[11] = bottomLeftEdge;
        assertTrue(Arrays.equals(expectedSolvedEdges, algo.generateSolvedEdgesWithPositions()));
    }


    public void testGenerateEdgesWithPositions(){
        Edge3x3[] expectedEdgesWithPositions = new Edge3x3[12];
        Edge3x3 topFrontEdge = new Edge3x3(Color.rgb(255, 165, 0), Color.WHITE);
        topFrontEdge.setEdgePos(7);
        expectedEdgesWithPositions[0] = topFrontEdge;
        Edge3x3 topRightEdge = new Edge3x3(Color.RED, Color.WHITE);
        topRightEdge.setEdgePos(4);
        expectedEdgesWithPositions[1] = topRightEdge;
        Edge3x3 topBackEdge = new Edge3x3(Color.rgb(255, 165, 0), Color.GREEN);
        topBackEdge.setEdgePos(11);
        expectedEdgesWithPositions[2] = topBackEdge;
        Edge3x3 topLeftEdge = new Edge3x3(Color.YELLOW, Color.RED);
        topLeftEdge.setEdgePos(5);
        expectedEdgesWithPositions[3] = topLeftEdge;
        Edge3x3 frontRightEdge = new Edge3x3(Color.RED, Color.GREEN);
        frontRightEdge.setEdgePos(9);
        expectedEdgesWithPositions[4] = frontRightEdge;
        Edge3x3 rightBackEdge = new Edge3x3(Color.YELLOW, Color.rgb(255, 165, 0));
        rightBackEdge.setEdgePos(6);
        expectedEdgesWithPositions[5] = rightBackEdge;
        Edge3x3 backLeftEdge = new Edge3x3(Color.GREEN, Color.WHITE);
        backLeftEdge.setEdgePos(8);
        expectedEdgesWithPositions[6] = backLeftEdge;
        Edge3x3 leftFrontEdge = new Edge3x3(Color.YELLOW, Color.GREEN);
        leftFrontEdge.setEdgePos(10);
        expectedEdgesWithPositions[7] = leftFrontEdge;
        Edge3x3 bottomFrontEdge = new Edge3x3(Color.YELLOW, Color.BLUE);
        bottomFrontEdge.setEdgePos(2);
        expectedEdgesWithPositions[8] = bottomFrontEdge;
        Edge3x3 bottomRightEdge = new Edge3x3(Color.RED, Color.BLUE);
        bottomRightEdge.setEdgePos(1);
        expectedEdgesWithPositions[9] = bottomRightEdge;
        Edge3x3 bottomBackEdge = new Edge3x3(Color.WHITE, Color.BLUE);
        bottomBackEdge.setEdgePos(0);
        expectedEdgesWithPositions[10] = bottomBackEdge;
        Edge3x3 bottomLeftEdge = new Edge3x3(Color.BLUE, Color.rgb(255, 165, 0));
        bottomLeftEdge.setEdgePos(3);
        expectedEdgesWithPositions[11] = bottomLeftEdge;
        algo.generateEdgesWithPositions();
        for (int i = 0; i < expectedEdgesWithPositions.length; i++){
            assertTrue(expectedEdgesWithPositions[i].equalsWithPos(cube.getEdges()[i]));
        }

    }


    public void testGetEdgeSwaps(){
        assertEquals(45, algo.getEdgeSwaps());
    }


}
