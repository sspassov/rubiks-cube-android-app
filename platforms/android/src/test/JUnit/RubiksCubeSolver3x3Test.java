package test.JUnit;

import com.example.rubiks.Concrete.RubiksCubeSolver3x3;

import junit.framework.TestCase;

/**
 * Created by sspas on 11/20/2016.
 */
public class RubiksCubeSolver3x3Test extends TestCase {

    RubiksCubeSolver3x3 solver = new RubiksCubeSolver3x3();

    public void testIsRubiksCubeSolvable() throws Exception {
        String[] topColors = new String[]{"blue", "blue", "blue", "blue", "blue", "blue", "blue", "blue", "blue"};
        String[] bottomColors = new String[]{"green", "green", "green", "green", "green", "green", "green", "green", "green"};
        String[] frontColors = new String[]{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"};
        String[] backColors = new String[]{"white", "white", "white", "white", "white", "white", "white", "white", "white"};
        String[] leftColors = new String[]{"red", "red", "red", "red", "red", "red", "red", "red", "red"};
        String[] rightColors = new String[]{"orange", "orange", "orange", "orange", "orange", "orange", "orange", "orange", "orange"};
        assertTrue(solver.isRubiksCubeSolvable(topColors, bottomColors, frontColors, backColors, leftColors, rightColors));
    }

    public void testIsRubiksCubeNotSolvable() throws Exception{
        String[] topColors = new String[]{"blue", "blue", "blue", "blue", "blue", "blue", "blue", "blue", "blue"};
        String[] bottomColors = new String[]{"green", "green", "green", "green", "green", "green", "green", "green", "green"};
        String[] frontColors = new String[]{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "orange", "yellow"};
        String[] backColors = new String[]{"white", "white", "white", "white", "white", "white", "white", "white", "white"};
        String[] leftColors = new String[]{"red", "red", "red", "red", "red", "red", "red", "red", "red"};
        String[] rightColors = new String[]{"orange", "orange", "orange", "orange", "orange", "orange", "orange", "yellow", "orange"};
        assertFalse(solver.isRubiksCubeSolvable(topColors, bottomColors, frontColors, backColors, leftColors, rightColors));
    }
}