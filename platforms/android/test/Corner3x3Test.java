package test;

import android.graphics.Color;

import com.example.rubiks.Concrete.Corner3x3;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by sspas on 10/7/2016.
 */
public class Corner3x3Test {

    @Test
    public void testTwoCornersAreEqual(){
        Corner3x3 firstCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        Corner3x3 secondCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        assertTrue(firstCorner.equals(secondCorner));
    }

    @Test
    public void testTwoCornersAreNotEqual(){
        Corner3x3 firstCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        Corner3x3 secondCorner = new Corner3x3(Color.GREEN, Color.BLUE, Color.YELLOW);
        assertFalse(firstCorner.equals(secondCorner));
    }

    @Test
    public void testTwoCornersAreSame(){
        Corner3x3 firstCorner = new Corner3x3(Color.YELLOW, Color.BLUE, Color.RED);
        Corner3x3 secondCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        assertTrue(firstCorner.isSameCorner(secondCorner));
    }

    @Test
    public void testTwoCornersAreNotSame(){
        Corner3x3 firstCorner = new Corner3x3(Color.YELLOW, Color.GREEN, Color.RED);
        Corner3x3 secondCorner = new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW);
        assertFalse(firstCorner.isSameCorner(secondCorner));
    }
}
