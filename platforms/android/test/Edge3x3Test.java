package test;

import android.graphics.Color;

import com.example.rubiks.Concrete.Edge3x3;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by sspas on 10/7/2016.
 */
public class Edge3x3Test {

    @Test
    public void testTwoEdgesAreEqual(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        assertTrue(firstEdge.equals(secondEdge));
    }

    @Test
    public void testTwoEdgesAreNotEqual(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        Edge3x3 secondEdge = new Edge3x3(Color.GREEN, Color.BLUE);
        assertFalse(firstEdge.equals(secondEdge));
    }

    @Test
    public void testTwoEdgesAreSame(){
        Edge3x3 firstEdge = new Edge3x3(Color.BLUE, Color.RED);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        assertTrue(firstEdge.isSameEdge(secondEdge));
    }

    @Test
    public void testTwoEdgesAreNotSame(){
        Edge3x3 firstEdge = new Edge3x3(Color.BLUE, Color.RED);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.GREEN);
        assertFalse(firstEdge.isSameEdge(secondEdge));
    }

    @Test
    public void testTwoEdgesAreEqualWithPos(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        firstEdge.setEdgePos(1);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        secondEdge.setEdgePos(1);
        assertTrue(firstEdge.equalsWithPos(secondEdge));
    }

    @Test
    public void testTwoEdgesAreNotEqualWithPos(){
        Edge3x3 firstEdge = new Edge3x3(Color.RED, Color.BLUE);
        firstEdge.setEdgePos(1);
        Edge3x3 secondEdge = new Edge3x3(Color.RED, Color.BLUE);
        secondEdge.setEdgePos(2);
        assertFalse(firstEdge.equalsWithPos(secondEdge));
    }
}
