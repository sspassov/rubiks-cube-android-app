package test;

import android.graphics.Color;

import com.example.rubiks.Concrete.EdgeParitySubAlgo;
import com.example.rubiks.Concrete.RubiksCube3x3;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by sspas on 10/15/2016.
 */
public class EdgeParitySubAlgoTest {

    @Test
    public void getEdgeSwapsTest(){
        int[] topColors = new int[]{Color.rgb(255, 165, 0), Color.WHITE, Color.YELLOW, Color.RED, Color.BLUE, Color.rgb(255, 165, 0), Color.GREEN, Color.YELLOW, Color.YELLOW};
        int[] bottomColors = new int[]{Color.rgb(255, 165, 0), Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN, Color.WHITE, Color.rgb(255, 165, 0), Color.BLUE, Color.rgb(255, 165, 0)};
        int[] frontColors = new int[]{Color.WHITE, Color.BLUE, Color.RED, Color.WHITE, Color.YELLOW, Color.rgb(255, 165, 0), Color.WHITE, Color.RED, Color.RED};
        int[] backColors = new int[]{Color.RED, Color.RED, Color.BLUE, Color.YELLOW, Color.WHITE, Color.RED, Color.GREEN, Color.WHITE, Color.GREEN};
        int[] leftColors = new int[]{Color.YELLOW, Color.YELLOW, Color.RED, Color.BLUE, Color.RED, Color.rgb(255, 165, 0), Color.YELLOW, Color.rgb(255, 165, 0), Color.BLUE};
        int[] rightColors = new int[]{Color.GREEN, Color.YELLOW, Color.BLUE, Color.GREEN, Color.rgb(255, 165, 0), Color.GREEN, Color.WHITE, Color.GREEN, Color.WHITE};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        EdgeParitySubAlgo algo = new EdgeParitySubAlgo();
        assertEquals(algo.getEdgeSwaps(cube), 7);
    }

    @Test
    public void checkEdgeParityTrue(){
        int[] topColors = new int[]{Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE};
		int[] bottomColors = new int[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN};
		int[] frontColors = new int[]{Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW};
		int[] backColors = new int[]{Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
		int[] leftColors = new int[]{Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED};
		int[] rightColors = new int[]{Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        EdgeParitySubAlgo algo = new EdgeParitySubAlgo();
        assertTrue(algo.checkEdgeParity(cube));
    }

    @Test
    public void checkEdgeParityFalse(){
        int[] topColors = new int[]{Color.rgb(255, 165, 0), Color.WHITE, Color.YELLOW, Color.RED, Color.BLUE, Color.rgb(255, 165, 0), Color.GREEN, Color.YELLOW, Color.YELLOW};
        int[] bottomColors = new int[]{Color.rgb(255, 165, 0), Color.GREEN, Color.BLUE, Color.BLUE, Color.GREEN, Color.WHITE, Color.rgb(255, 165, 0), Color.BLUE, Color.rgb(255, 165, 0)};
        int[] frontColors = new int[]{Color.WHITE, Color.BLUE, Color.RED, Color.WHITE, Color.YELLOW, Color.rgb(255, 165, 0), Color.WHITE, Color.RED, Color.RED};
        int[] backColors = new int[]{Color.RED, Color.RED, Color.BLUE, Color.YELLOW, Color.WHITE, Color.RED, Color.GREEN, Color.WHITE, Color.GREEN};
        int[] leftColors = new int[]{Color.YELLOW, Color.YELLOW, Color.RED, Color.BLUE, Color.RED, Color.rgb(255, 165, 0), Color.YELLOW, Color.rgb(255, 165, 0), Color.BLUE};
        int[] rightColors = new int[]{Color.GREEN, Color.YELLOW, Color.BLUE, Color.GREEN, Color.rgb(255, 165, 0), Color.GREEN, Color.WHITE, Color.GREEN, Color.WHITE};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        EdgeParitySubAlgo algo = new EdgeParitySubAlgo();
        assertFalse(algo.checkEdgeParity(cube));
    }
}
