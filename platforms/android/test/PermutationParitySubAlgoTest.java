package test;

import android.graphics.Color;

import com.example.rubiks.Concrete.PermutationParitySubAlgo;
import com.example.rubiks.Concrete.RubiksCube3x3;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by sspas on 10/7/2016.
 */
public class PermutationParitySubAlgoTest {

    @Test
    public void checkPermutationParityTestTrue(){
        int[] topColors = new int[]{Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.YELLOW, Color.YELLOW, Color.BLUE, Color.RED, Color.YELLOW, Color.rgb(255, 165, 0), Color.WHITE};
        int[] bottomColors = new int[]{Color.YELLOW, Color.YELLOW, Color.WHITE, Color.BLUE, Color.GREEN, Color.RED, Color.rgb(255, 165, 0), Color.WHITE, Color.RED};
        int[] frontColors = new int[]{Color.RED, Color.WHITE, Color.BLUE, Color.YELLOW, Color.WHITE, Color.RED, Color.RED, Color.BLUE, Color.RED};
        int[] backColors = new int[]{Color.rgb(255, 165, 0), Color.GREEN, Color.BLUE, Color.YELLOW, Color.YELLOW, Color.GREEN, Color.BLUE, Color.BLUE, Color.WHITE};
        int[] leftColors = new int[]{Color.YELLOW, Color.RED, Color.BLUE, Color.WHITE, Color.rgb(255, 165, 0), Color.GREEN, Color.GREEN, Color.rgb(255, 165, 0), Color.GREEN};
        int[] rightColors = new int[]{Color.rgb(255, 165, 0), Color.WHITE, Color.GREEN, Color.GREEN, Color.RED, Color.rgb(255, 165, 0), Color.GREEN, Color.BLUE, Color.WHITE};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        PermutationParitySubAlgo algo = new PermutationParitySubAlgo();
        assertTrue(algo.checkPermutationParity(cube));
    }

    @Test
    public void checkPermutationParityTestFalse(){
		int[] topColors = new int[]{Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE};
		int[] bottomColors = new int[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN};
		int[] frontColors = new int[]{Color.YELLOW, Color.rgb(255, 165, 0), Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW};
		int[] backColors = new int[]{Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
		int[] leftColors = new int[]{Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED};
		int[] rightColors = new int[]{Color.rgb(255, 165, 0), Color.YELLOW, Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
        RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);
        PermutationParitySubAlgo algo = new PermutationParitySubAlgo();
        assertFalse(algo.checkPermutationParity(cube));
    }
}
