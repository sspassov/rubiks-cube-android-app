package test;

import android.graphics.Color;

import com.example.rubiks.Concrete.Corner3x3;
import com.example.rubiks.Concrete.Edge3x3;
import com.example.rubiks.Concrete.RubiksCube3x3;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by sspas on 10/5/2016.
 */
public class RubiksCube3x3Test {

    int[] topColors = new int[]{Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE, Color.BLUE};
    int[] bottomColors = new int[]{Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN, Color.GREEN};
    int[] frontColors = new int[]{Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW, Color.YELLOW};
    int[] backColors = new int[]{Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE};
    int[] leftColors = new int[]{Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED, Color.RED};
    int[] rightColors = new int[]{Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0), Color.rgb(255, 165, 0)};
    RubiksCube3x3 cube = new RubiksCube3x3(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);

    @Test
    public void generateSidesTest(){
        assertEquals(cube.getTopSide().getSideColor(), Color.BLUE);
        assertEquals(cube.getBottomSide().getSideColor(), Color.GREEN);
        assertEquals(cube.getFrontSide().getSideColor(), Color.YELLOW);
        assertEquals(cube.getBackSide().getSideColor(), Color.WHITE);
        assertEquals(cube.getLeftSide().getSideColor(), Color.RED);
        assertEquals(cube.getRightSide().getSideColor(), Color.rgb(255, 165, 0));
    }

    @Test
    public void generateCornersTest(){
        assertEquals(cube.getCorners()[0], new Corner3x3(Color.WHITE, Color.BLUE, Color.RED));
        assertEquals(cube.getCorners()[1], new Corner3x3(Color.rgb(255, 165, 0), Color.BLUE, Color.WHITE));
        assertEquals(cube.getCorners()[2], new Corner3x3(Color.RED, Color.BLUE, Color.YELLOW));
        assertEquals(cube.getCorners()[3], new Corner3x3(Color.YELLOW, Color.BLUE, Color.rgb(255, 165, 0)));
        assertEquals(cube.getCorners()[4], new Corner3x3(Color.RED, Color.GREEN, Color.WHITE));
        assertEquals(cube.getCorners()[5], new Corner3x3(Color.WHITE, Color.GREEN, Color.rgb(255, 165, 0)));
        assertEquals(cube.getCorners()[6], new Corner3x3(Color.YELLOW, Color.GREEN, Color.RED));
        assertEquals(cube.getCorners()[7], new Corner3x3(Color.rgb(255, 165, 0), Color.GREEN, Color.YELLOW));
    }

    @Test
    public void generateEdgesTest(){
        assertEquals(cube.getEdges()[0], new Edge3x3(Color.BLUE, Color.YELLOW));
        assertEquals(cube.getEdges()[1], new Edge3x3(Color.BLUE, Color.rgb(255, 165, 0)));
        assertEquals(cube.getEdges()[2], new Edge3x3(Color.BLUE, Color.WHITE));
        assertEquals(cube.getEdges()[3], new Edge3x3(Color.BLUE, Color.RED));
        assertEquals(cube.getEdges()[4], new Edge3x3(Color.YELLOW, Color.rgb(255, 165, 0)));
        assertEquals(cube.getEdges()[5], new Edge3x3(Color.WHITE, Color.rgb(255, 165, 0)));
        assertEquals(cube.getEdges()[6], new Edge3x3(Color.WHITE, Color.RED));
        assertEquals(cube.getEdges()[7], new Edge3x3(Color.YELLOW, Color.RED));
        assertEquals(cube.getEdges()[8], new Edge3x3(Color.GREEN, Color.YELLOW));
        assertEquals(cube.getEdges()[9], new Edge3x3(Color.GREEN, Color.rgb(255, 165, 0)));
        assertEquals(cube.getEdges()[10], new Edge3x3(Color.GREEN, Color.WHITE));
        assertEquals(cube.getEdges()[11], new Edge3x3(Color.GREEN, Color.RED));
    }
}
