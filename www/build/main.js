window.cube;
var firstTime = true;
$(document).ready( function(){

	document.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);
	
	var controls = ERNO.Locked;

	var ua = navigator.userAgent,
		isIE = ua.indexOf('MSIE') > -1 || ua.indexOf('Trident/') > -1;

	window.cube = new ERNO.Cube({
		hideInvisibleFaces: true,
		controls: controls,
		renderer: isIE?ERNO.renderers.IeCSS3D : null

	});


	cube.addEventListener('click', function( event ){
		var stickerColor = '';
		var radios = document.getElementsByName('color');
		for(var i = 0; i < radios.length; i++){
			if (radios[i].checked){
				stickerColor = radios[i].id.toString();
			}
		}
		if(stickerColor != ''){
			var cubelet = event.detail.cubelet;
			var face = event.detail.face;
			face = ( face === 'TOP' ) ? 'UP' : face
			var element = cubelet [face.toLowerCase()].element;
			var sticker = 'sticker ' + stickerColor;
			element.querySelector('.sticker').className = sticker;
		}
	});

	cube.camera.setLens(30);

	var container = document.getElementById('container');
	container.appendChild(cube.domElement);
});

function solveCube(){
	//Getting the top colors
	var topColors = [];
	for (var i = 0; i < 9; i++){
		topColors[i] = window.cube.faces[1].cubelets[i].up.element.querySelector('.sticker').className.split(' ')[1];
		// console.log(window.cube.faces[1].cubelets[i].up.element.querySelector('.sticker'));
	}
	var bottomColors = [];
	for (var i = 0; i < 9; i++){
		bottomColors[i] = window.cube.faces[3].cubelets[i].down.element.querySelector('.sticker').className.split(' ')[1];
		// console.log(window.cube.faces[3].cubelets[i].down.element.querySelector('.sticker'));
	}
	var frontColors = [];
	for (var i = 0; i < 9; i++){
		frontColors[i] = window.cube.faces[0].cubelets[i].front.element.querySelector('.sticker').className.split(' ')[1];
		// console.log(window.cube.faces[0].cubelets[i].front.element.querySelector('.sticker'));
	}
	var backColors = [];
	for (var i = 0; i < 9; i++){
		backColors[i] = window.cube.faces[5].cubelets[i].back.element.querySelector('.sticker').className.split(' ')[1];
		//console.log(window.cube.faces[5].cubelets[i].back.element.querySelector('.sticker'));
	}
	var leftColors = [];
	for (var i = 0; i < 9; i++){
		leftColors[i] = window.cube.faces[4].cubelets[i].left.element.querySelector('.sticker').className.split(' ')[1];
		// console.log(window.cube.faces[4].cubelets[i].left.element.querySelector('.sticker'));
	}
	var rightColors = [];
	for (var i = 0; i < 9; i++){
		rightColors[i] = window.cube.faces[2].cubelets[i].right.element.querySelector('.sticker').className.split(' ')[1];
		//console.log(window.cube.faces[2].cubelets[i].right.element.querySelector('.sticker'));
	}
	if(window.Solver.isRubiksCubeSolvable(topColors, bottomColors, frontColors, backColors, leftColors, rightColors)){
		searchForSolution(topColors, bottomColors, frontColors, backColors, leftColors, rightColors);

	}else{
		var message = document.getElementById('outputMessage');
			message.innerHTML = "The cube you have created is not solvable";
	}

	
}

function searchForSolution(topColors, bottomColors, frontColors, backColors, leftColors, rightColors){
	//Identifying the color of each side 
	//Cannot do that from the cube color property since it doesn't change when you rotate the cube
	if(firstTime){
		var message = document.getElementById('outputMessage');
			message.innerHTML = "Please wait while I find the perfect solution";
	}


	topColor = topColors[4];
	bottomColor = bottomColors[4];
	frontColor = frontColors[4];
	backColor = backColors[4];
	leftColor = leftColors[4];
	rightColor = rightColors[4];
	allColors = [topColor, bottomColor, frontColor, backColor, leftColor, rightColor];
	test = transformColorsToSidesComplex(leftColors, allColors, true);
	sideNotationFaceles = "";
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesStandard(topColors, allColors));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesStandard(rightColors, allColors));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesStandard(frontColors, allColors));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesComplex(bottomColors, allColors, false));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesComplex(leftColors, allColors, true));
	sideNotationFaceles = sideNotationFaceles.concat(transformColorsToSidesComplex(backColors, allColors, true));
	
	if(sideNotationFaceles.length == 54){
		if (firstTime){
			var solution = window.Searcher.solutionSearch(sideNotationFaceles, 30, 10, false);
			message.innerHTML = solution;
			firstTime = false;

		}else{
			var message = document.getElementById('outputMessage');
			message.innerHTML = "";
			var solution = window.Searcher.solutionSearch(sideNotationFaceles, 30, 10, false);
			message.innerHTML = solution;
		}	
	}

}

//Transforms top colors to side notation
//used for the twophase library
//Two different transformations will be used for the different sides
function transformColorsToSidesStandard(sideColors, allColors){
	transformedColors = "";
	for(var i = 8; i >= 0; i--){
		if(sideColors[i] === allColors[0]){
			transformedColors = transformedColors.concat("U");
		}else if(sideColors[i] === allColors[1]){
			transformedColors = transformedColors.concat("D");
		}else if(sideColors[i] === allColors[2]){
			transformedColors = transformedColors.concat("F");
		}else if(sideColors[i] === allColors[3]){
			transformedColors = transformedColors.concat("B");
		}else if(sideColors[i] === allColors[4]){
			transformedColors = transformedColors.concat("L");
		}else if(sideColors[i] === allColors[5]){
			transformedColors = transformedColors.concat("R");
		}
	}

	return transformedColors;
}

//Since some of the indexes in the chromes cube mismatch the indexes in the 
// twophase algo we need to do some complex transformation
//we need to treat the sideColors as an 2D array of colors
// the following function will apply the following transformation
// 0 1 2    2 5 8 
// 3 4 5 -> 1 4 7 	
// 6 7 8    0 3 6
//if we invert the loops the following transformation will be applied
// 0 1 2    6 3 0 
// 3 4 5 -> 7 4 1 	
// 6 7 8    8 5 2

function transformColorsToSidesComplex(sideColors, allColors, invertLoops){
	sideColors2D = [sideColors.slice(0, 3), sideColors.slice(3, 6), sideColors.slice(6, 9)];
	rows = sideColors2D.length;
	columns = sideColors2D[0].length;
	transformedColors = "";

	for (var column = 0; column < columns; column++){
		for(var row = 0; row < rows; row++){
			realRow = row;
			realCol = column;
			if(invertLoops){
				realRow = (rows-1) - realRow;
			}else{
				realCol = (columns-1) - realCol;
			}

			if(sideColors2D[realRow][realCol] === allColors[0]){
				transformedColors = transformedColors.concat("U");
			}else if(sideColors2D[realRow][realCol] === allColors[1]){
				transformedColors = transformedColors.concat("D");
			}else if(sideColors2D[realRow][realCol] === allColors[2]){
				transformedColors = transformedColors.concat("F");
			}else if(sideColors2D[realRow][realCol] === allColors[3]){
				transformedColors = transformedColors.concat("B");
			}else if(sideColors2D[realRow][realCol] === allColors[4]){
				transformedColors = transformedColors.concat("L");
			}else if(sideColors2D[realRow][realCol] === allColors[5]){
				transformedColors = transformedColors.concat("R");
			}
		}
	}

	return transformedColors;
}

//Simple function that resets the cube to a solved state
function resetCube(){

	// Create new cube and add listeners
	var controls = ERNO.Locked;

	var ua = navigator.userAgent,
		isIE = ua.indexOf('MSIE') > -1 || ua.indexOf('Trident/') > -1;

	window.cube = new ERNO.Cube({
		hideInvisibleFaces: true,
		controls: controls,
		renderer: isIE?ERNO.renderers.IeCSS3D : null

	});


	cube.addEventListener('click', function( event ){
		var stickerColor = '';
		var radios = document.getElementsByName('color');
		for(var i = 0; i < radios.length; i++){
			if (radios[i].checked){
				stickerColor = radios[i].id.toString();
			}
		}
		if(stickerColor != ''){
			var cubelet = event.detail.cubelet;
			var face = event.detail.face;
			face = ( face === 'TOP' ) ? 'UP' : face
			var element = cubelet [face.toLowerCase()].element;
			var sticker = 'sticker ' + stickerColor;
			element.querySelector('.sticker').className = sticker;
		}
	});

	cube.camera.setLens(30);

	//Remove old cube and replace with the new one
	var container = document.getElementById('container');
	container.removeChild(container.childNodes[0]);
	container.appendChild(cube.domElement);
}